package TestCase;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseTest {
	
	
	
	public static WebDriver driver;
	
	
	
	
	@BeforeSuite
	
	public void initSuite()
	{
		//System.setProperty("webdriver.chrome.driver", "C:\\Users\\A\\Desktop\\Selenium\\chromedriver.exe");
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
			driver.manage().window().maximize();
			
		driver.get("https://opensource-demo.orangehrmlive.com/");
	}
	
	@AfterSuite
	public void tearDown()
	{
		driver.close();
	}

}
