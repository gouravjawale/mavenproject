package Pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;



public class LoginPage {

	
	
	WebDriver driver;
	@FindBy (xpath= "//*[@id='txtUsername']")
	private WebElement username;
	
	@FindBy (xpath= "//*[@id='txtPassword']")
	private WebElement password;
	 
	@FindBy (xpath= "//*[@id='btnLogin']")
	private WebElement loginBtn;
	 

	public LoginPage(WebDriver driver) 
	{
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	
	
	public void enteringCredentials(String un,String pwd)
	{
		
	
	username.sendKeys(un);
	password.sendKeys(pwd);
	}
	
	public void loginWithCorrectCred()
	{ 
		loginBtn.click();
	}
	
	
}
